<?php

use App\Model\Kelas;
use Illuminate\Database\Seeder;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kelas::create([
            'kode_kelas' => 'TInf1',
            'nama_kelas' => '4B Reguler'
            ]);
    }
}
