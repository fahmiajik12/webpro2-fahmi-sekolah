<?php

use App\Model\Siswa;
use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Siswa::create([
            'nis' => '2005040059',
            'nama' => 'Fahmi Aji Kurnia',
            'gender' => 'laki-laki',
            'tempat_lahir' => 'Magelang',
            'tgl_lahir' => '12',
            'email' => 'Fahmiaji123.fak@gmail.com',
            'nama_ortu' => 'Imam Ks',
            'alamat' => 'Magelang',
            'phone_number' => '088225443321',
            'kelas_id' => '001'
            ]);
    }
}
