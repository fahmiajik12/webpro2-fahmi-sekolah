<?php

use App\Model\Guru;
use Illuminate\Database\Seeder;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Guru::create([
            'user_id' => '001',
            'nip' => 'Pemrograman Web 1',
            'nama' => 'Fahmi Aji Kurnia',
            'tempat_lahir' => 'Magelang',
            'tgl_lahir' => '12',
            'gender' => 'laki-laki',
            'phone_number' => '085161412071',
            'email' => 'Fahmiaji123.fak@gmail.com',
            'alamat' => 'Magelang',
            'pendidikan' => 'mahasiswa'
            ]);
    }
}
